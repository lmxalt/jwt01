CREATE DATABASE jwt;

-- Расширение - create extension of not exist "uuid-ossp";

CREATE TABLE users(
    user_id uuid /*uuid - идентификатор*/ PRIMARY KEY DEFAULT
    uuid_generate_v4(),
    user_name VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    user_password VARCHAR(255) NOT NULL
);

-- Users

INSERT INTO users (user_name, user_email, user_password) VALUES 